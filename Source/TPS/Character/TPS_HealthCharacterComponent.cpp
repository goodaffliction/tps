// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_HealthCharacterComponent.h"

void UTPS_HealthCharacterComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0)
		{
			//FX
			//UE_LOG(LogTem, Warning, TEXT("UTPS_HealthCharacterComponent::ChangeHealthValue: Shiled < 0.0f"));
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UTPS_HealthCharacterComponent::GetCurrentShield()
{
	return Shield;
}

void UTPS_HealthCharacterComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	ShieldChangeEvent_Multicast(Shield, ChangeValue);
	//OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownShieldTimer, this,
			&UTPS_HealthCharacterComponent::CooldownShieldEnd, CooldownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	
}

void UTPS_HealthCharacterComponent::CooldownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this,
			&UTPS_HealthCharacterComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTPS_HealthCharacterComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

float UTPS_HealthCharacterComponent::GetShieldValue()
{
	return Shield;
}

void UTPS_HealthCharacterComponent::ShieldChangeEvent_Multicast_Implementation(float newShield, float Damage)
{
	OnShieldChange.Broadcast(newShield, Damage);
}
