// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Weapon/WeaponDefault.h"
#include "Character/TPSInventoryComponent.h"
#include "Character/TPS_HealthCharacterComponent.h"
#include "Interface/TPS_IGameInterface.h"
#include "Character/StateEffect.h"
#include "TPSCharacter.generated.h"


UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameInterface
{
	GENERATED_BODY()

public:
	ATPSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	// InvComp
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = InventoryComponent, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HealthComponent, meta = (AllowPrivateAccess = "true"))
		class UTPS_HealthCharacterComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UDecalComponent* CurrentCursor = nullptr;

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	//MovementState
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	

	//Effect
	UPROPERTY(Replicated)
		TArray<UStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UStateEffect* EffectRemove = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UStateEffect> AbilityEffect;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;

	//Weapon
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	UFUNCTION()
		void InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		FName InitWeaponName;

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}

	//CharacterInputs
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();

	void TryAbilityEnabled();

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//Tick Function
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Functions
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	UFUNCTION()
		void DropCurrentWeapon();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, 
		class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable)
		void CharDead();
	UFUNCTION(BlueprintCallable)
		bool GetIsAlive();
	UFUNCTION(NetMulticast, Reliable)
		void EnableRagdoll_Multicast();

	//Inteface
	EPhysicalSurface GetSurfuceType() override;
	TArray<UStateEffect*> GetAllCurrentEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UStateEffect* RemoveEffect);
		void RemoveEffect_Implementation(UStateEffect* RemoveEffect)override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UStateEffect* newEffect);
		void AddEffect_Implementation(UStateEffect* newEffect)override;

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();

	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
		void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* Anim);

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
		void SwitchEffect(UStateEffect* Effect, bool bIsAdd);
};

