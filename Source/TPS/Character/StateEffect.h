// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TPS_API UStateEffect : public UObject
{
	GENERATED_BODY()

public:

	bool IsSupportedForNetworking()const override { return true; };
	virtual bool InitObject(AActor* ActorFName, FName NameBonehit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bIsStacable = false;

	AActor* myActor = nullptr;
	UPROPERTY(Replicated)
	FName NameBone;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bIsAutoDestroyParticleEffect = false;

//	UParticleSystemComponent* ParticleEmitter = nullptr;

	UFUNCTION(NetMulticast, Reliable)
		void FXSpawnByStateEffect_Multicast(UParticleSystem* Effect, FName NameBoneHit);
};

UCLASS()
class TPS_API UStateEffect_ExecuteOnce : public UStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBonehit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingExecuteOnce")
		float Power = 20.0f;
};

UCLASS()
class TPS_API UStateEffect_ExecuteTimer : public UStateEffect
{
	GENERATED_BODY()

public:
	
	bool InitObject(AActor* Actor, FName NameBonehit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingExecute")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingExecute")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingExecute")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingExecute")
		UParticleSystem* ParticleEffect = nullptr;*/

	UParticleSystemComponent* ParticleEmitter = nullptr;
};