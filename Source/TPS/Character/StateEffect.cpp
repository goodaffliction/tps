// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect.h"
#include "TPS_HealthComponent.h"
#include "Interface/TPS_IGameInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

bool UStateEffect::InitObject(AActor* Actor, FName NameBonehit)
{
	myActor = Actor;
	NameBone = NameBonehit;
	ITPS_IGameInterface* myInterface = Cast<ITPS_IGameInterface>(myActor);
	if (myInterface)
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	return true;
}

void UStateEffect::DestroyObject()
{
	ITPS_IGameInterface* myInterface = Cast<ITPS_IGameInterface>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

void UStateEffect::FXSpawnByStateEffect_Multicast_Implementation(UParticleSystem* Effect, FName NameBoneHit)
{
	//if (Effect)
	//{
	//	FName NameBoneToAttached = NameBoneHit;
	//	FVector Loc = FVector(0);


	//	USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	//	if (myMesh)
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect,
	//			myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator,
	//			EAttachLocation::SnapToTarget, false);
	//	}
	//	else
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect,
	//			myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator,
	//			EAttachLocation::SnapToTarget, false);
	//	}
	//}
}

bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBonehit)
{
	Super::InitObject(Actor, NameBonehit);
	ExecuteOnce();
	return true;
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPS_HealthComponent* MyHealthComp = Cast<UTPS_HealthComponent>(myActor->GetComponentByClass(UTPS_HealthComponent::StaticClass()));
		if (MyHealthComp)
		{
			MyHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}
		DestroyObject();
}

bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBonehit)
{
	Super::InitObject(Actor, NameBonehit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	//ParticleEmitter->DestroyComponent();
	//ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPS_HealthComponent* MyHealthComp = Cast<UTPS_HealthComponent>(myActor->GetComponentByClass(UTPS_HealthComponent::StaticClass()));
		if (MyHealthComp)
		{
			MyHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}
}

void UStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UStateEffect, NameBone);
}

