// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TPS.h"
#include "Character/StateEffect.h"
#include "Interface/TPS_IGameInterface.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UStateEffect* myEffect = Cast<UStateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsCanAdd = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStacable)
						{
							int8 j = 0;
							TArray<UStateEffect*> CurrentEffects;
							ITPS_IGameInterface* myInterface = Cast<ITPS_IGameInterface>(TakeEffectActor);
							if (myInterface)
							{
								CurrentEffects = myInterface->GetAllCurrentEffects();
							}

							if (CurrentEffects.Num() > 0)
							{
								while (j < CurrentEffects.Num() && !bIsCanAddEffect)
								{
									if (CurrentEffects[j]->GetClass() != AddEffectClass)
									{
										bIsCanAddEffect = true;
									}
									j++;
								}
							}
							else
							{
								bIsCanAddEffect = true;
							}
						}
					else 
					{
						bIsCanAddEffect = true;
					}
					if (bIsCanAddEffect)
					{
						bIsCanAdd = true;
						UStateEffect* NewEffect = NewObject<UStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}
	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* target, FVector offset, FName Socket)
{
	if (target)
	{
		FName SocketToAttached = Socket;
		FVector Loc = offset;
		ACharacter* myCharacter = Cast<ACharacter>(target);
		if (myCharacter && myCharacter->GetMesh())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFX, myCharacter->GetMesh(), SocketToAttached, Loc,
				FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			if (target->GetRootComponent())
			{
				UGameplayStatics::SpawnEmitterAttached(ExecuteFX, target->GetRootComponent(), SocketToAttached, Loc,
					FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
}
