// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, 
	AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	TimerEnabledFalse_Multicast();
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		InitVisualExplose_Multicast(ProjectileSetting.ExploseFX);
	}
	if (ProjectileSetting.ExploseSound)
	{
		SpawnExploseSound_Multicast(ProjectileSetting.ExploseSound);
	}
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		1000.0f,
		2000.0f,
		5,
		NULL, IgnoredActor, this, nullptr);
	this->Destroy();
}

void AProjectileDefault_Grenade::TimerEnabledFalse_Multicast_Implementation()
{
	if (!TimerEnabled)
	{
		Explose();
	}
}

void AProjectileDefault_Grenade::InitVisualExplose_Multicast_Implementation(UParticleSystem* newTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), newTemplate,
		GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnExploseSound_Multicast_Implementation(USoundBase* HitSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());
}

void AProjectileDefault_Grenade::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AProjectileDefault_Grenade, TimerEnabled);
	DOREPLIFETIME(AProjectileDefault_Grenade, TimerToExplose);
	DOREPLIFETIME(AProjectileDefault_Grenade, TimeToExplose);
}
